import React, { useState } from "react";

import "./NewExpenseForm.css";

const NewExpenseForm = (props) => {
  const [currentTitle, setCurrentTitle] = useState("");
  const [currentAmount, setCurrentAmount] = useState("");
  const [currentDate, setCurrentDate] = useState("");

  const titleChangeHandler = (event) => {
    setCurrentTitle(event.target.value);
  };

  const amountChangeHandler = (event) => {
    setCurrentAmount(event.target.value);
  };

  const dateChangeHandler = (event) => {
    setCurrentDate(event.target.value);
  };

  const submitHandler = (event) => {
    event.preventDefault();
    const expenseData = {
      title: currentTitle,
      amount: +currentAmount,
      date: new Date(currentDate),
    };

    props.onSaveExpenseData(expenseData);

    setCurrentTitle("");
    setCurrentAmount("");
    setCurrentDate("");
  };

  return (
    <form onSubmit={submitHandler}>
      <div className="new-expense__controls">
        <div className="new-expense__control">
          <label>Title</label>
          <input
            type="text"
            onChange={titleChangeHandler}
            value={currentTitle}
          />
        </div>

        <div className="new-expense__control">
          <label>Amount</label>
          <input
            type="number"
            min="0.01"
            step="0.01"
            value={currentAmount}
            onChange={amountChangeHandler}
          />
        </div>

        <div className="new-expense__control">
          <label>Date</label>
          <input
            type="date"
            min="2019-01-01"
            onChange={dateChangeHandler}
            value={currentDate}
          />
        </div>

        <div className="new-expense__actions">
          <button type="submit">Add Expense</button>
          <button type="button" onClick={props.toggleFormFlag}>
            Cancel
          </button>
        </div>
      </div>
    </form>
  );
};

export default NewExpenseForm;
