import React, { useState } from "react";

import "./NewExpense.css";
import NewExpenseForm from "./NewExpenseForm";

const NewExpense = (props) => {
  const [expenseFormFlag, setExpenseFormFlag] = useState(false);

  const saveExpenseDataHandler = (enteredExpenseData) => {
    const expenseData = {
      ...enteredExpenseData,
      id: Math.random().toString(),
    };

    props.onAddExpense(expenseData);
    expenseFormFlagHanler();
  };

  const expenseFormFlagHanler = () => {
    setExpenseFormFlag((prevFlag) => {
      setExpenseFormFlag(!prevFlag);
    });
  };

  return (
    <div className="new-expense">
      {!expenseFormFlag && (
        <button onClick={expenseFormFlagHanler}>Add New Expense</button>
      )}
      {expenseFormFlag && (
        <NewExpenseForm
          onSaveExpenseData={saveExpenseDataHandler}
          toggleFormFlag={expenseFormFlagHanler}
        />
      )}
    </div>
  );
};

export default NewExpense;
