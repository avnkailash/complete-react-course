import { useState } from "react";
import Card from "../Card/Card";
import ExpenseItem from "../ExpenseItem/ExpenseItem";
import ExpenseFilter from "../ExpensesFilter/ExpensesFilter";
import ExpensesChart from "../Chart/ExpensesChart";

import "./ExpenseList.css";

const ExpenseList = ({ expenses }) => {
  const [filteredYear, setFilteredYear] = useState("2019");

  const filteredYearChangeHandler = (selectedYear) => {
    setFilteredYear(selectedYear);
  };

  const filteredExpenses = expenses.filter((expense) => {
    return expense.date.getFullYear().toString() === filteredYear;
  });

  return (
    <div>
      <Card className="expenses">
        <ExpenseFilter
          onFilteredYearChange={filteredYearChangeHandler}
          selectedYear={filteredYear}
        />
        <ExpensesChart expenses={filteredExpenses} />
        {filteredExpenses.length === 0 ? (
          <p>No Expenses Found for the year {filteredYear}</p>
        ) : (
          filteredExpenses.map((expense, index) => (
            <ExpenseItem
              title={expense.title}
              amount={expense.amount}
              date={expense.date}
              key={expense.id}
            />
          ))
        )}
      </Card>
    </div>
  );
};

export default ExpenseList;
