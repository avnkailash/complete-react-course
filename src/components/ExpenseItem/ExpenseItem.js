import React, { useState } from "react";
import ExpenseDate from "../ExpenseDate/ExpenseDate";
import "./ExpenseItem.css";

const ExpenseItem = (props) => {
  const [title, setTitle] = useState(props.title);
  // const [amount, setAmount] = useState(props.amount);

  const changeTitleHandler = () => setTitle("Updated!!");

  return (
    <div className="expense-item">
      <ExpenseDate date={props.date} />
      <div className="expense-item__description">
        <h2>{title}</h2>
        <div className="expense-item__price">${props.amount}</div>
        <button onClick={changeTitleHandler}>Change Title</button>
      </div>
    </div>
  );
};

export default ExpenseItem;
