import { useState } from "react";
import ExpenseList from "./components/ExpenseList/ExpenseList";
import NewExpense from "./components/NewExpense/NewExpense";

const INITIAL_EXPENSES = [
  {
    id: "e1",
    title: "Macbook Pro",
    amount: 3499,
    date: new Date(2021, 7, 14),
  },
  {
    id: "e2",
    title: "Asus ROG5 Mobile",
    amount: 1200,
    date: new Date(2021, 6, 5),
  },
  {
    id: "e3",
    title: "Play Station 5",
    amount: 800,
    date: new Date(2021, 9, 9),
  },
  {
    id: "e4",
    title: "L Shaped Office Table",
    amount: 600,
    date: new Date(2021, 7, 5),
  },
];

function App() {
  const [expenses, setExpenses] = useState(INITIAL_EXPENSES);

  const addExpenseHandler = (expense) => {
    setExpenses((prevState) => [expense, ...prevState]);
  };

  return (
    <div>
      <NewExpense onAddExpense={addExpenseHandler} />
      <ExpenseList expenses={expenses} />
    </div>
  );
}

export default App;
